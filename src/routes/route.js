const { Router } = require('express');
const router = Router();

const { getCategory, createCategoria } = require('../controllers/index.controller')

router.get('/category',getCategory);
router.post('/category',createCategoria);


module.exports = router;

